---
permalink: /
title: "Is your Bitcoin wallet secure?"
summary: "The aim of this project is to improve the security of Bitcoin wallets by examining the application code for possible back-doors and other vulnerabilities."
excerpt: "Many wallets are not open to public scrutiny"
author_profile: true
redirect_from:
  - /about/
  - /about.html
---

<script>
  window.wallets = {% include allAppList.html %}
</script>

<div class="page-section">

<h1 id="all-wallets-ordered-by-verifiability-downloads-and-ratings">What protects your Bitcoins from Hackers?</h1>

<img src="/images/hacker-bg.png" alt="hacker" style="height:10em;margin:0 0em 1em 0" />
<p>
Do <h>you</h> own your Bitcoins or do you <h>trust</h> that your app allows you to use "your"
coins while they are actually controlled by <h>"them"</h>? Do you have a backup? Do
"they" have a copy they didn't tell you about? Did anybody check the wallet for deliberate backdoors
or <h>vulnerabilities</h>? Could anybody check the wallet for those?
</p>
<p>
We try to answer these questions for Bitcoin wallets on Android.
</p>
<p>
Read about <a title="our methodology" href="/methodology/">our methodology</a> to understand in more detail.
</p>

</div>


<h2 class="section-label">As featured on:</h2>

{% include press.html %}


<h2 class="section-label">All wallets ordered by verifiability, downloads and ratings</h2>

{% include list_of_wallets.html %}


<h2 class="section-label">How many wallets are in each category?</h2>

{% include grid_of_wallets.html %}


<h2 class="section-label">How many users (downloads) are in each category?</h2>

{% include grid_of_wallets_proportional.html %}

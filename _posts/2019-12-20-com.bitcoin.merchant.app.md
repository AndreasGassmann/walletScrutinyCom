---
title: "Bitcoin Cash Register (BCH)"
altTitle: 

users: 50000
appId: com.bitcoin.merchant.app
launchDate: 2019-04-13
latestUpdate: 2019-11-21
apkVersionName: "4.4.1"
stars: 4.4
ratings: 235
reviews: 104
size: 5.1M
website: https://www.bitcoin.com/bitcoin-cash-register
repository: 
issue: 
icon: com.bitcoin.merchant.app.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-25
reviewStale: false
signer: 
reviewArchive:


providerTwitter: bitcoincom
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: btc

permalink: /posts/com.bitcoin.merchant.app/
redirect_from:
  - /com.bitcoin.merchant.app/
---


This is a watch-only wallet according to their description:

> Just enter either a standard Bitcoin Cash address or an “extended public key”
(aka an “xpub”) from your Bitcoin Cash wallet to start accepting instant and
secure Bitcoin Cash payments at your business.

As it doesn't manage private keys, you cannot spend with it and consequently
neither the provider can steal or lose your funds.

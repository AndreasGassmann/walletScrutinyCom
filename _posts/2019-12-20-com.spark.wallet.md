---
title: "Spark Lightning Wallet"
altTitle: 

users: 500
appId: com.spark.wallet
launchDate: 2018-12-10
latestUpdate: 2019-12-15
apkVersionName: "0.0.0"
stars: 4.6
ratings: 7
reviews: 3
size: 6.5M
website: https://github.com/shesek/spark-wallet
repository: 
issue: 
icon: com.spark.wallet.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-29
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.spark.wallet/
redirect_from:
  - /com.spark.wallet/
---



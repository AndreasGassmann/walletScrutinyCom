---
title: "Badger Wallet"
altTitle: 

users: 10000
appId: com.badgermobile
launchDate: 2019-06-10
latestUpdate: 2020-03-04
apkVersionName: "1.12.1"
stars: 4.0
ratings: 73
reviews: 37
size: 10M
website: https://badger.bitcoin.com
repository: 
issue: 
icon: com.badgermobile.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: badgerwallet
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: 

permalink: /posts/com.badgermobile/
redirect_from:
  - /com.badgermobile/
---



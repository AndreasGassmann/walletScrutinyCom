---
title: "XZEN — Bitcoin Wallet and Exchange"
altTitle: 

users: 5000
appId: com.xzen.wallet
launchDate: 
latestUpdate: 2019-09-19
apkVersionName: "1.2.6.0"
stars: 3.9
ratings: 37
reviews: 29
size: 26M
website: http://www.xzen.io/app
repository: 
issue: 
icon: com.xzen.wallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: XZENwallet
providerLinkedIn: 
providerFacebook: xzenwallet
providerReddit: 

permalink: /posts/com.xzen.wallet/
redirect_from:
  - /com.xzen.wallet/
---


This app claims:

> In XZEN only you will have access to your account. You'll be able to manage it
  in your own way and feel no worries about any blocks.

which can be interpreted as a claim to not being custodial but also as a claim
of their accounts being somehow very secure. It's not worded clearer in other
parts, which makes it look like this could be a custodial product.

The provider also ~~sells~~ offers for preorder a hardware wallet, the "Xzen
Wallet" ... [projected to be released "beginning of 2020"](https://www.xzen.io/faq).

Absent public source code, claims cannot be verified anyway what gets us to the
verdict **not verifiable**.

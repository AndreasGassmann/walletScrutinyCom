---
title: "PayWay Wallet"
altTitle: 

users: 10000
appId: com.paywaywallet
launchDate: 2019-02-13
latestUpdate: 2020-05-26
apkVersionName: "3.0.2 (build: #1014/20200526140535)"
stars: 3.6
ratings: 137
reviews: 84
size: 14M
website: http://www.payway.ug
repository: 
issue: 
icon: com.paywaywallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: pay_way
providerLinkedIn: 
providerFacebook: paywayuganda
providerReddit: 

permalink: /posts/com.paywaywallet/
redirect_from:
  - /com.paywaywallet/
  - /payway/
---


This app looks like BitRefill in its features and also its property as a wallet:
It's not advertised as being a wallet but in the screenshots you see an available
balance in crypto currencies, so it is also a wallet.

As the app requires login and password, it looks like an interface to a
custodial service.

[Their FAQ](https://help.payway.ug/wallet/faq/) features weird answers in form
of screenshots that don't answer the question. "What is the maximum amount I can
send?" Answer: Some numbers without unit.

Either way, daily limits are not common in actual non-custodial wallets, so again
this looks custodial.

Our verdict: **not verifiable**.

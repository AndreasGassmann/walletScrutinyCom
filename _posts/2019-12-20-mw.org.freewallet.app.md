---
title: "Bitcoin and Crypto Blockchain Wallet by Freewallet"
altTitle: 

users: 100000
appId: mw.org.freewallet.app
launchDate: 2017-08-10
latestUpdate: 2020-07-15
apkVersionName: "1.11.0"
stars: 4.2
ratings: 5549
reviews: 3510
size: 12M
website: https://freewallet.org/
repository: 
issue: 
icon: mw.org.freewallet.app.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

permalink: /posts/mw.org.freewallet.app/
redirect_from:
  - /mw.org.freewallet.app/
---


According to the description

> In addition, the majority of cryptocurrency assets on the platform are stored in an offline vault. Your coins will be kept in cold storage with state of the art security protecting them.

this is a custodial app.

Our verdict: **not verifiable**.

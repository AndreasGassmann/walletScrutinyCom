---
title: "PolisPay - Cryptocurrency wallet"
altTitle: 

users: 5000
appId: com.polispay.copay
launchDate: 2018-02-21
latestUpdate: 2020-06-23
apkVersionName: "8.6.0"
stars: 4.3
ratings: 109
reviews: 64
size: 19M
website: https://www.polispay.com/
repository: 
issue: 
icon: com.polispay.copay.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.polispay.copay/
redirect_from:
  - /com.polispay.copay/
---


This app appears to be a [CoPay](/copay/) clone given its app ID:
`com.polispay.copay`.

In the app's description we read:

> A unique wallet based on mnemonic phrases and public and private extended keys
> PolisPay wallet is impossible to hack.

which sounds like a claim to be non-custodial. Let's see if there is source code
somewhere ... nope. No mention of its source code or a repository.

This app is **not verifiable**.


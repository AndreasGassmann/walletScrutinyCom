---
title: "Uphold: buy and sell Bitcoin"
altTitle: 

users: 1000000
appId: com.uphold.wallet
launchDate: 
latestUpdate: 2020-07-10
apkVersionName: "4.8.8"
stars: 3.6
ratings: 6426
reviews: 3469
size: 53M
website: 
repository: 
issue: 
icon: com.uphold.wallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.uphold.wallet/
redirect_from:
  - /com.uphold.wallet/
---


This app appears to be an interface to a custodial trading platform. In the
Google Play description we read:

> Trust Through Transparency
> Uphold is fully reserved. Unlike banks, we don’t loan out your money. To prove
  it, we publish our holdings in real time.

If they hold your money, you don't. As a custodial service this app is **not
verifiable**.